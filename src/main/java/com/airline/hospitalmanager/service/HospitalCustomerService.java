package com.airline.hospitalmanager.service;

import com.airline.hospitalmanager.entity.HospitalCustomer;
import com.airline.hospitalmanager.model.HospitalCustomerRegister;
import com.airline.hospitalmanager.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HospitalCustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setHospitalCustomer(HospitalCustomerRegister register) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(register).build();


        hospitalCustomerRepository.save(addData);
    }
    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }
}
