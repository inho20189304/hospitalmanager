package com.airline.hospitalmanager.service;

import com.airline.hospitalmanager.entity.HospitalCustomer;
import com.airline.hospitalmanager.entity.MedicalRecord;
import com.airline.hospitalmanager.model.MedicalRecordItem;
import com.airline.hospitalmanager.model.MedicalRecordRequest;
import com.airline.hospitalmanager.repository.MedicalRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicalRecordService {
    private final MedicalRecordRepository medicalRecordRepository;

    public void setMedicalRecord(HospitalCustomer hospitalCustomer, MedicalRecordRequest medicalRecordRequest) {
        MedicalRecord addData = new MedicalRecord.MedicalRecordBuilder(hospitalCustomer, medicalRecordRequest).build();

        medicalRecordRepository.save(addData);
    }
    public List<MedicalRecordItem> getRecordByDate(LocalDate searchDate) {
        List<MedicalRecord> originList = medicalRecordRepository.findAllByStartCureDateOrderByIdDesc(searchDate);

        List<MedicalRecordItem> result = new LinkedList<>();

        for (MedicalRecord item : originList) {
            MedicalRecordItem addItem = new MedicalRecordItem.MedicalRecordItemBuilder(item).build();

            result.add(addItem);
        }
        return result;
    }
    public void putPayment(long id) {
        MedicalRecord originData = medicalRecordRepository.findById(id).orElseThrow();
        originData.putPayment();

        medicalRecordRepository.save(originData);
    }
}
