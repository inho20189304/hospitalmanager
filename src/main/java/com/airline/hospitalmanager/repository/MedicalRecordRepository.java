package com.airline.hospitalmanager.repository;

import com.airline.hospitalmanager.entity.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Long> {
    List<MedicalRecord> findAllByStartCureDateOrderByIdDesc(LocalDate startCureDate);
}
