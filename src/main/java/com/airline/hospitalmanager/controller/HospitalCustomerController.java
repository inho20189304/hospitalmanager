package com.airline.hospitalmanager.controller;

import com.airline.hospitalmanager.model.HospitalCustomerRegister;
import com.airline.hospitalmanager.service.HospitalCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class HospitalCustomerController {

    private final HospitalCustomerService hospitalCustomerService;

    @PostMapping("/register")
    public String setHospitalCustomer(@RequestBody @Valid HospitalCustomerRegister register) {
        hospitalCustomerService.setHospitalCustomer(register);

        return "ok";
    }
}
