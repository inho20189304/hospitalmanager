package com.airline.hospitalmanager.controller;

import com.airline.hospitalmanager.entity.HospitalCustomer;
import com.airline.hospitalmanager.entity.MedicalRecord;
import com.airline.hospitalmanager.model.MedicalRecordItem;
import com.airline.hospitalmanager.model.MedicalRecordRequest;
import com.airline.hospitalmanager.service.HospitalCustomerService;
import com.airline.hospitalmanager.service.MedicalRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/record")
public class MedicalRecordController {
    private final HospitalCustomerService hospitalCustomerService;
    private final MedicalRecordService medicalRecordService;
    @PostMapping("/new/customer-id/{customerId}")
    public String  setMedicalRecord(@PathVariable long customerId, @RequestBody @Valid MedicalRecordRequest request) {
        HospitalCustomer hospitalCustomer = hospitalCustomerService.getData(customerId);
        medicalRecordService.setMedicalRecord(hospitalCustomer, request);

        return "ok";
    }
    @GetMapping("/all/data")
    public List<MedicalRecordItem> getRecordBYDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd"
    )LocalDate searchDate) {
        return medicalRecordService.getRecordByDate(searchDate);
    }
    @PutMapping("/payment/record-id/recordId")
    public String putPayment(@PathVariable long recordId) {
        medicalRecordService.putPayment(recordId);

        return "ok";
    }
}
