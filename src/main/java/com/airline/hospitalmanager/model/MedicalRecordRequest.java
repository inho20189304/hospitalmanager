package com.airline.hospitalmanager.model;

import com.airline.hospitalmanager.enums.TreatmentItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MedicalRecordRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private TreatmentItem treatmentItem;
    @NotNull
    private Boolean isInsurance;
}
