package com.airline.hospitalmanager.model;

import com.airline.hospitalmanager.entity.MedicalRecord;
import com.airline.hospitalmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MedicalRecordItem {
    private Long recordId;
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String uniqueNumber;
    private String treatmentName;
    private Double cost;
    private String isInsurance;
    private LocalDate startCureDate;
    private LocalTime startCureTime;
    private String payment;

    private MedicalRecordItem(MedicalRecordItemBuilder builder) {
        this.recordId = builder.recordId;
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.uniqueNumber = builder.uniqueNumber;
        this.treatmentName = builder.treatmentName;
        this.cost = builder.cost;
        this.isInsurance = builder.isInsurance;
        this.startCureDate = builder.startCureDate;
        this.startCureTime = builder.startCureTime;
        this.payment = builder.payment;
    }
    public static class MedicalRecordItemBuilder implements CommonModelBuilder<MedicalRecordItem> {
        private final Long recordId;
        private final Long customerId;
        private final String customerName;
        private final String customerPhone;
        private final String uniqueNumber;
        private final String treatmentName;
        private final Double cost;
        private final String isInsurance;
        private final LocalDate startCureDate;
        private final LocalTime startCureTime;
        private final String payment;

        public MedicalRecordItemBuilder(MedicalRecord medicalRecord) {
            this.recordId = medicalRecord.getId();
            this.customerId = medicalRecord.getHospitalCustomer().getId();
            this.customerName = medicalRecord.getHospitalCustomer().getCustomerName();
            this.customerPhone = medicalRecord.getHospitalCustomer().getCustomerPhone();
            this.uniqueNumber = medicalRecord.getHospitalCustomer().getUniqueNumber();
            this.treatmentName = medicalRecord.getTreatmentItem().getTreatmentName();
            this.cost = medicalRecord.getCost();
            this.isInsurance = medicalRecord.getIsInsurance() ? "예" : "아니요";
            this.startCureDate = medicalRecord.getStartCureDate();
            this.startCureTime = medicalRecord.getStartCureTime();
            this.payment = medicalRecord.getPayment() ? "예" : "아니요";
        }
        @Override
        public MedicalRecordItem build() {
            return new MedicalRecordItem(this);
        }
    }
}
