package com.airline.hospitalmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TreatmentItem {
    REHABILITATION("재활치료", 500000, 800000),
    PHYSICALTHERAPY("물리치료", 100000, 150000),
    SAP("수액", 30000, 50000),
    ACUPUNCTURE("침", 50000, 100000),
    CHIROPRACTIC("척추교정", 250000,500000)
    ;
    private final String treatmentName;
    private final double insuranceApply;
    private final double insuranceUnapply;
}
