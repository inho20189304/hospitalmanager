package com.airline.hospitalmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
