package com.airline.hospitalmanager.entity;

import com.airline.hospitalmanager.interfaces.CommonModelBuilder;
import com.airline.hospitalmanager.model.HospitalCustomerRegister;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HospitalCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 13)
    private String customerPhone;
    @Column(nullable = false, length = 14)
    private String uniqueNumber;
    @Column(nullable = false, length = 100)
    private String address;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String reasonForVisit;
    @Column(nullable = false)
    private LocalDate firstVisit;

    private HospitalCustomer(HospitalCustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.uniqueNumber = builder.uniqueNumber;
        this.address = builder.address;
        this.reasonForVisit = builder.reasonForVisit;
        this.firstVisit = builder.firstVisit;
    }

    public static class HospitalCustomerBuilder implements CommonModelBuilder<HospitalCustomer> {
        private final String customerName;
        private final String customerPhone;
        private final String uniqueNumber;
        private final String address;
        private final String reasonForVisit;
        private final LocalDate firstVisit;

        public HospitalCustomerBuilder(HospitalCustomerRegister register) {
            this.customerName = register.getCustomerName();
            this.customerPhone = register.getCustomerPhone();
            this.uniqueNumber = register.getUniqueNumber();
            this.address = register.getAddress();
            this.reasonForVisit = register.getReasonForVisit();
            this.firstVisit = LocalDate.now();
        }

        @Override
        public HospitalCustomer build() {
            return new HospitalCustomer(this);
        }
    }
}
