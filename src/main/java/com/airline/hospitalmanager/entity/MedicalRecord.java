package com.airline.hospitalmanager.entity;

import com.airline.hospitalmanager.enums.TreatmentItem;
import com.airline.hospitalmanager.interfaces.CommonModelBuilder;
import com.airline.hospitalmanager.model.MedicalRecordRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MedicalRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalCustomerId", nullable = false)
    private HospitalCustomer hospitalCustomer;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TreatmentItem treatmentItem;
    @Column(nullable = false)
    private Boolean isInsurance;
    @Column(nullable = false)
    private Double cost;
    @Column(nullable = false)
    private LocalDate startCureDate;
    @Column(nullable = false)
    private LocalTime startCureTime;
    @Column(nullable = false)
    private Boolean payment;

    public void putPayment() {
        this.payment = true;
    }

    private MedicalRecord(MedicalRecordBuilder builder) {
        this.hospitalCustomer = builder.hospitalCustomer;
        this.treatmentItem = builder.treatmentItem;
        this.isInsurance = builder.isInsurance;
        this.cost = builder.cost;
        this.startCureDate = builder.startCureDate;
        this.startCureTime = builder.startCureTime;
        this.payment = builder.payment;

    }

    public static class MedicalRecordBuilder implements CommonModelBuilder<MedicalRecord> {
        private final HospitalCustomer hospitalCustomer;
        private final TreatmentItem treatmentItem;
        private final Boolean isInsurance;
        private final Double cost;
        private final LocalDate startCureDate;
        private final LocalTime startCureTime;
        private final Boolean payment;

        public MedicalRecordBuilder(HospitalCustomer customer, MedicalRecordRequest request) {
            this.hospitalCustomer = customer;
            this.treatmentItem = request.getTreatmentItem();
            this.isInsurance = request.getIsInsurance();
            this.cost = request.getIsInsurance() ? request.getTreatmentItem().getInsuranceApply() :
                    request.getTreatmentItem().getInsuranceUnapply();
            this.startCureDate = LocalDate.now();
            this.startCureTime = LocalTime.now();
            this.payment = false;
        }


        @Override
        public MedicalRecord build() {
            return new MedicalRecord(this);
        }
    }

}
